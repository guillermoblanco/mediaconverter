'use-strict';
//Entry point, set custom object in global
global.GLOB = {};
global.GLOB['dstPath'] = null;
global.GLOB['ROOT_PATH'] = __dirname;
global.GLOB['PLATFORM'] = /^win/.test( process.platform ) ? 'windows' : process.platform;

var bindings = require('./node/bindings.js');
var fs 		= require('fs');
var async   = require('async');
// ***************** Electron *****************
var app           = require('app');             // Module to control application life.
var BrowserWindow = require('browser-window');  // Module to create native browser window.
// ********************************************

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is GCed.
var mainWindow     = null;

var install = function( setup ) {

	mainWindow.loadUrl('file://' + global.GLOB.ROOT_PATH + '/html/install.html');

	var p = global.GLOB.PLATFORM;
	var installer = require('./node/downloader.js')( p, setup.ffmpeg[p], __dirname, 2 );

	// Logs when there is a new state to be done
	installer.on( 'newState', function( evt, name ) {
		var txt = evt + 'ing: ' + name + '...';
		mainWindow.send('updateInstallState', String(txt));
	});
	// Logs when the current state is done
	installer.on( 'stateDone', function( evt, name ) {
		var txt = evt + 'ing: ' + name + ' - done';
		mainWindow.send('updateInstallState', String(txt));
	});
	// Logs current state percent
	installer.on( 'stateProgress', function( evt, name, percent ) {
		var txt = evt + 'ing: ' + name + ' - ' + percent;
		mainWindow.send('updateInstallState', String(txt));
	});
	// Logs current global progress
	installer.on( 'progress', function( percent ) {
		mainWindow.send('installProgress', percent);
	});
	// Logs when everything is finished
	installer.on( 'done', function() {
		installer = null;
		setup.ffmpeg.downloaded = true;
		fs.writeFileSync( __dirname + '/lib/setup.json', JSON.stringify(setup) );
		mainWindow.loadUrl('file://' + global.GLOB.ROOT_PATH + '/html/index.html');
	});
	// Logs when there is an error
	installer.on( 'err', function( err ) {
		installer = null;
		mainWindow.loadUrl('file://' + global.GLOB.ROOT_PATH + '/html/error.html');
	});
};


// Quit when all windows are closed.
app.on('window-all-closed', app.quit);

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function() {
    // Create the browser window.
    mainWindow = new BrowserWindow({width: 800, height: 600});
    // Associate bindings
    bindings.setup( mainWindow );

    // TODO => UI COMM INSTALLATION PROGRESS
    // Check if setup is needed
	fs.readFile(__dirname + '/lib/setup.json', function( err, file ) {
		if ( err ) {
			mainWindow.loadUrl('file://' + global.GLOB.ROOT_PATH + '/html/error.html');
			mainWindow.send('error', err);
		} else {
			var setup = JSON.parse( file.toString('utf8') );
			if ( setup.ffmpeg.downloaded ) {
				needsSetup = false;
				mainWindow.loadUrl('file://' + global.GLOB.ROOT_PATH + '/html/index.html');
			} else {
				install( setup );
			}
		}
	});

    //mainWindow.openDevTools();
    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });
});