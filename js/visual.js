var ipc    = require('ipc');
var $ 	   = require('jquery');
var _	   = require('lodash');

var addItemToFileList = function ( item, idx ) {
	//console.log(item);
	var table = $('<div class="row"></div>');
	var p  = $('<div class="col-sm-6"><p>' + item.vPath + '</p></div>');

	table.append(p);

	var dropdown = $('<div class="col-sm-6 dropdown"></div>');
	var button 	 = $('<button'
						+' id="' + idx +'"  '
						+' type="button" '
						+' class="btn btn-primary dropdown-toggle"'
						+' data-toggle="dropdown" '
						+' aria-haspopup="true" '
						+' aria-expanded="false">'+
					'Format...</button>');
	var span = $('<span class="caret"></span>');
	var options = $('<div class="dropdown-menu" aria-labelledby="dropdownMenu1"></div>');
	item.formats.forEach( function ( fmt, index ) {
		var tmp = $('<button type="button"'
						+' class="btn btn-warning" '
						+' onclick="fmtBtnUpdate($(this).data())">'
						+ fmt + 
					'</button>').data({fmt:fmt, item: item, idx: idx});
		options.append(tmp);
	});
	if ( item.formats.length ) {
		var tmp = $('<button type="button" class="btn btn-default" onclick="reqMoreFmts($(this).data())">More</button>')
					.data({idx:idx, type: item.type});
		options.append()
	}
	button.append(span);
	dropdown.append(button);
	dropdown.append(options);

	table.append(dropdown);

	var li = $('<li class="list-group-item" id="li-'+ idx + '"></li>');
	item['idx'] = idx;
	li.data(item);
	li.append(table);
	$('#file-list').append(li);
};

var setProgressBar = function ( idx, percent ) {
	var progHolder = $('<div id="' + 'progHolder-' + idx + '" class="progress"></div>');
	var progBar    = $('<div '
						+ 'id="' + 'progBar-' + idx + '" '
						+ 'class="progress-bar progress-bar-success '
						+ 'role="progressbar" '
						+ 'aria-valuenow="0" '
						+ 'aria-valuemin="0" '
						+ 'aria-valuemax="100" '
						+ 'style="min-width: 2em; width: 0%;">' +
						'</div>').data({count: 0});
	if ( percent === 'UNAVAILABLE' ) {
		progBar.addClass("progress-bar-striped");
	}
	progHolder.append(progBar);
	$($('#file-list').children().get(idx)).append(progHolder);
};

var updateProgressBar = function ( idx, percent ) {
	if ( percent === 'UNAVAILABLE' ) {
		percent = $(('#progBar-' + idx)).data()['count'] + 10;
	}
	if ( percent !== undefined ) {
		$(('#progBar-' + idx)).css('width', percent + '%').attr('aria-valuenow', percent);
	}
};

var setState = function ( state, idx ) {
	var col  = $('<div class="col-sm-6"></div>');
	var item = null;
	if ( state === 'err') {
		item = $('<span id="' + 'err-' + idx + '" class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>');
	} else {
		item = $('<span id="' + 'ok-' + idx + '" class="glyphicon glyphicon-ok" aria-hidden="true"></span>');
	}
	col.append(item);
	$(('#progHolder-' + idx)).remove();
	$($(('#li-' + idx)).children().get(0)).append(col);
};

var reqMoreFmts = function( data ) {
	ipc.send('reqMoreFmts', data);
};

var setupDstFolder = function( data, override ) {
	override = override || true;
	if ( $('#dst-path').data().hasOwnProperty('dst-path') ) {
		if ( override ) {
			$('#dst-path').val(data);
			$('#dst-path').data({'dst-path': data});
		}
	} else {
		$('#dst-path').val(data);
		$('#dst-path').data({'dst-path': data});
	}
};

var formatTheRest = function( ext, fmt ) {
	// For each file with same extension and no fmt chosen, override it
	$('#file-list').children().each(function( idx, item ) {
		var txt = $(item).data().vPath.split('/');
		var extension = txt[txt.length-1].split('.');
		extension = extension[extension.length-1];
		if ( !($('#' + idx).data()).hasOwnProperty('fmt') && extension === ext ) {
			updateBtn( idx, fmt );
		}
	});
};

var updateBtn = function( idx, fmt ) {
	$(('#' + idx)).text(fmt);
	$(('#' + idx)).removeClass('btn btn-primary dropdown-toggle').addClass('btn btn-warning dropdown-toggle')
	var span = $('<span class="caret"></span>');
	$(('#' + idx)).append(span);
	$(('#' + idx)).data({fmt: fmt});
};

var fmtBtnUpdate = function( event ) {
	updateBtn( event.idx, event.fmt );
	var ext = event.item.vPath.split('/');
	ext = ext[ext.length-1].split('.');
	formatTheRest(ext[ext.length-1], event.fmt );
};

var ID = 0;
// ********************* BINDINGS *********************
document.addEventListener( 'dragover', function ( event ) {
	event.preventDefault();
	ipc.send('dragover');
	return false;
}, false);

document.addEventListener( 'drop', function ( event ) {
	event.preventDefault();
	$('body').removeClass('drop');
	$('header').remove();
	var files = [];
	for (var i = 0; i < event.dataTransfer.files.length; ++i) {
		files.push(event.dataTransfer.files[i].path);
	}
	ipc.send('drop', files);
	var p = event.dataTransfer.files[0].path.split('/');
	p.pop();
	setupDstFolder(p.join('/'), false);
	return false;
}, false);

$('#dst-path').on('change', function() {
	ipc.send('folderChange', $('#dst-path').val());
});

$('#conv-btn').on('click', function() {
	var acc = [];
	// Check format chosen
	var id = $($('#file-list').children().get(0)).data().idx;
	if ( !($('#' + id).data().hasOwnProperty('fmt')) ) {
		alert('Choose a format first!');
	} else {
		$('#file-list').children().each(function( idx, elem ) {
			// Extract index of list_item, path to file and fmt
			var data = _.pick($('#li-' + idx).data(), ['idx', 'path']);
			data['fmt'] = $(('#' + idx)).data().fmt;
			// Only for files with format
	        if ( data.fmt !== undefined ) {
				acc.push(data);
				setProgressBar(idx, 'UNAVAILABLE');
			}
		});
		$('#conv-btn').addClass('disabled');
		$('#clear-btn').addClass('disabled');
		ipc.send('convert', acc);
	}
});

$('#folder-btn').on('click', function() {
	ipc.send('showFolderDialog');
});

$('#clear-btn').on('click', function() {
	$('#file-list').empty();
	ID = 0;
});

ipc.on('stat-result', function( data ) {
	console.log('stat-result');
	data.forEach( function ( item, idx ) {
		// Prepare path
		var p = item.file.split('/');
		addItemToFileList({ 
			vPath: ('../' + p[p.length-2] + '/' + p[p.length-1]), 
			formats: item.formats,
			path: item.file,
			type: item.type
		}, ID );
		ID += 1;
	});
	$('#controls').show();
});

ipc.on('folderDialog', setupDstFolder);

ipc.on('updateItem', function( evt, idx, data ) {
	switch ( evt ) {
		case 'progress':
			updateProgressBar( idx, data );
			break;
		case 'end':
		case 'errored':
			setState( evt, idx );
			break;
		default: break;
	}
});

ipc.on('conversion-end', function() {
	$('#conv-btn').removeClass('disabled');
	$('#clear-btn').removeClass('disabled');
});

/*ipc.on('moreFmts', function( data ) {
	$(('#' + data.idx)).
});*/
// ****************************************************
 
 
 
 
 
 
 