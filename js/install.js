var $	 = require('jQuery');
var ipc  = require('ipc');

var updateProgressBar = function ( percent ) {
	if ( percent !== undefined ) {
		$('#installProgBar').css('width', parseInt(Math.floor(percent), 10) + '%').attr('aria-valuenow', parseInt(Math.floor(percent), 10));
	}
};

var updateState = function ( txt ) {
	$("#stateText").html(txt);
};

ipc.on( 'installProgress', updateProgressBar );
ipc.on( 'updateInstallState', updateState );
