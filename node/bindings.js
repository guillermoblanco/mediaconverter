var ipc         = require('ipc');
var dialog      = require('dialog');
var queue       = require('./utils.js').queue;
var mainWindow  = null;

// *************** BINDINGS ***************
    // ********* CONVERTER *********
ipc.on('drop', function( e, data ) {
    console.log(data);
    //console.log('drop', e.sender.getUrl());
    queue.checkFile(data);
    if ( global.GLOB.dstPath !== null ) {
        mainWindow.send( 'folderDialog', global.GLOB.dstPath );
    }
});

ipc.on('convert', function( e, data ) {
    // https://github.com/fluent-ffmpeg/node-fluent-ffmpeg
    console.log('convert', data);
    /*console.log('dst:',global.GLOB.dstPath);*/
    data.forEach( function( item, idx ) {
        // Extract name
        var tmp = item.path.split('/');
        tmp = tmp[tmp.length-1];
        // Extract extension
        tmp = tmp.split('.');
        tmp.pop();
        // Add new extension
        tmp += '.' + item.fmt;

        var task = {
            idx  : item.idx,
            file : item.path,
            dst  : (global.GLOB.dstPath + '/' + tmp),
            fmt  : item.fmt
        }
        console.log('Going to push', task);
        queue.push(task, function(){console.log('pusher fn', arguments);});
    });
});

ipc.on('folderChange', function( e, path ) {
    global.GLOB.dstPath = path;
    console.log('folderChange', global.GLOB.dstPath);
});

ipc.on( 'showFolderDialog', function(){
    var result = dialog.showOpenDialog( mainWindow, { properties : [ 'openDirectory' ] } )[0];
    if ( result && result !== global.GLOB.dstPath ) {
        global.GLOB.dstPath = result;
    } else {
        result = global.GLOB.dstPath;
    }
    mainWindow.send( 'folderDialog', result || [] );
});
// ****************************************

module.exports = {
    setup: function( window ) {
        mainWindow = window;
        queue.setCallback( mainWindow.send, mainWindow );
    }
}