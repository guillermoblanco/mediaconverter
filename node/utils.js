'use-strict';

var fs            = require('fs');
var async         = require('async');
var ffmpeg        = require('fluent-ffmpeg');
var spawn         = require('child_process').spawn;
var windowCb      = function() {};

// **************** CONFIG ****************
var FFMPEG_PATH   = null;
var FFPROBE_PATH  = null;
(function () {
    var FFM_PATH  = global.GLOB.ROOT_PATH + '/lib/ffmpeg/' + global.GLOB.PLATFORM;
    var suffix    = (global.GLOB.PLATFORM === 'windows') ? '.exe' : '';
    FFMPEG_PATH   = FFM_PATH + '/ffmpeg' + suffix;
    FFPROBE_PATH  = FFM_PATH + '/ffprobe' + suffix;
})();
// ****************************************

var videoRgxp = /(webm|mkv|flv|vob|ogv|ogg|gif|gifv|mng|avi|mov|qt|wmv|yuv|rm|rmvb|asf|mp4|m4p|m4v|mpg|mp2|mpeg|mpe|mpv|mpg|mpeg|m2v|m4v|svi|3gp|3g2|mxf|roq|nsv)/;
var audioRgxp = /(3gp|act|act|aiff|aac|amr|ape|au|awb|dct|dss|dvf|iklax|ivs|mmf|msv|flac|m4a|m4p|mp3|mpc|oga|vox|wav|wma|wv|webm)/;
var formats   =  {
    video: {
        common: [
            'mp4','webm','flv','ogg','gif','avi','mov','mpeg','mpg','mpeg'
        ],
        other: [
            'vob','ogv','mkv','gifv','mng','qt','wmv','yuv','rm','rmvb','asf','mpg','m4p',
            'm4v','mp2','mpe','mpv','m2v','m4v','svi','3gp','3g2','mxf','roq','nsv'
        ]
    },
    audio: {
        common: [
            'm4a','mp3','wav','wma','aiff','aac','flac','webm'
        ],
        other: [
            'amr','ape','au','awb','dct','dss','dvf','iklax','3gp','act','act','ivs','mmf','msv','m4p','mpc','oga','vox','wv'
        ]
    } 
};

// ************** UTILS ***************

// ************************************

// *********** QUEUE UTILS *************
var _timeFormatter = function( time ) {
    time = time.split(':');
    var ret = 0;
    for (var i = 0; i < time.length; i++) {
        ret += (time[i] * Math.pow(60, (time.length - 1 - i)));
    }
    return ret;
};

var _probeFile = function( file, callback ) {

    var extractor = /.*Duration: (([0-9]{2}:?)+)/gi;
    // FFM.. lib gives data to stderr
    var probe = spawn(FFPROBE_PATH, ['-i', file]);
    var dur = null;

    probe.stderr.on('data', function( data ) {
        dur = extractor.exec(data.toString('utf8'));
    });

    probe.on('close', function() {
        if ( !dur ) {
            return callback('FFPROBE ERR');
        }
        callback(null, dur[1]);
    });
};

var _converter = function( item, callback ) {

    var ERR = 'WRONG PARAMS', duration = null;

    if ( !item.hasOwnProperty('file') || !item.hasOwnProperty('dst') || !item.hasOwnProperty('idx') ) {
        return callback( ERR );
    }

    // Probe file to get duration
    _probeFile( item.file, function( err, data ) {
        if ( err || data === 'WRONG ARGS') {
            console.log('FFPROBE ERR', (err || data));
        } else {
            duration = _timeFormatter( data );
        }
    });

    // Convert file
    var p = ffmpeg( item.file )
        .setFfmpegPath( FFMPEG_PATH )
        .setFfprobePath( FFPROBE_PATH )
        .format(item.fmt)
        .audioChannels(2)
        .on('progress', function( info ) {
            var progress = info.percent;
            if ( !progress ) {
                if ( duration ) {
                    progress = (_timeFormatter(info.timemark)*100)/duration;
                } else {
                    progress = 'UNAVAILABLE';
                }
            }
            progress = parseFloat(progress);
            console.log('progress:', progress);
            // Communicate to window
            windowCb('updateItem', 'progress', item.idx, progress.toFixed(2));
        })
        .on('end', function() {
            // Communicate to window
            windowCb('updateItem', 'end', item.idx);
            return callback('ok');
        })
        .on('error', function(err, stdout, stderr) {
            // Communicate to window
            windowCb('updateItem', 'errored', item.idx);
            fs.writeFileSync( global.GLOB.dstPath + '/err_log.txt', 'err: {\n' + (err || '') + '\n}\n stdout: {\n' + (stdout || '') + ' \n}\n stderr: {\n' + (stderr || '') + '\n}\n');
            return callback(('err', stdout, stderr) );
        })
        .save(item.dst);
};
// ************************************

// BAD FUNCTION 
// given a directory, checks wether the files are suitable for conversion
// also, sets dstPath and communicate results to window, 
// returns array of files with their available conversion formats
var checkFile = function ( path ) {

    var to = [];

    async.each(path, function ( path, callback ) {

        async.waterfall([

            function ( callback ) {
                fs.lstat(path, callback);
            },

            function ( stat, callback ) {
                if ( stat.isDirectory() ) {
                    fs.readdir( path, function( err, files ) {
                        var f = [];
                        files.forEach(function(item) {
                            console.log('pusher', item, item.indexOf('.'));
                            // Avoid "hidden files"
                            if ( item.indexOf('.') !== 0 ) {
                                f.push(path + '/' + item);
                            }
                        });
                        callback(null, f);
                    });
                } else if ( stat.isFile() ) {
                    callback(null, [path]);
                } else {
                    callback('file-list-error');
                }
            }

        ], function( err, result ) {
            if ( err ) {
                to.push({ dst: 'file-list-error' , file: (err === 'file-list-error') ? null : err });
            } else {
                result.forEach(function(item) {
                    // Prepare formats, check if is video or audio, copy all attributes form formats
                    var test;
                    var rFmt =  [];
                    var type = '';
                    if ( (test = audioRgxp.exec(item)) !== null ) {
                        // DEEP COPY, must exist other way
                        rFmt = JSON.parse(JSON.stringify(formats.audio.common));
                        rFmt.splice(rFmt.indexOf( test[0] ), 1);
                        type = 'audio';
                    } else if ( (test = videoRgxp.exec(item)) !== null ) {
                        // DEEP COPY, must exist other way
                        rFmt = JSON.parse(JSON.stringify(formats.video.common));
                        rFmt.splice(rFmt.indexOf( test[0] ), 1);
                        type = 'video';
                    } else {
                        rFmt.push('');
                    }
                    //console.log('formats:', rFmt);
                    to.push({ dst: 'file-list' , file: item, formats: rFmt, type: type });
                    if ( global.GLOB.dstPath === null ) {
                        var p = item.split('/');
                        p.pop();
                        global.GLOB.dstPath = p.join('/');
                    }
                });
            }
            callback(null);
        });

    }, function() {
        // Communicate to window
        windowCb('stat-result', to);
    });
};

var queue = async.queue(_converter, 10);
queue.drain = function() {
    console.log('Drain', arguments);
    // Communicate to window
    windowCb('conversion-end');
};

queue.setCallback = function( callback, ctx ) {
    if ( typeof callback === 'function' ) {
        windowCb = function() {
            callback.apply(ctx, arguments);
        }
    }
};

queue.checkFile = checkFile;

module.exports.queue = queue;