'use-strict';
var fs 		= require('fs');
var async   = require('async');
var AdmZip  = require('adm-zip');
var events	= require('events');
var spawn	= require('child_process').spawn;

// *************** UTILS ***************
var mapToRange = function( num, from, to ) {
	// A: lower current bound
	// B: upper current bound
	// C: lower global bound
	// D: upper global bound
	// Y = (X-A)/(B-A) * (D-C) + C
	return ( (num - from[0])/(from[1] - from[0]) * (to[1] - to[0]) + to[0] );
}
// *************************************

/**
 * Downloader
 * Events:
 * 	newState, params: evt, name 				Logs when there is a new state to be done
 * 	stateDone, params: evt, name				Logs when the current state is done
 * 	stateProgress, params: evt, name, percent 	Logs current state percent
 * 	progress, params: percent					Logs current global progress
 * 	done, params: null							Logs when everything is finished
 * 	err, params: err 							Logs when there is an error
 */
var Downloader = function( platform, url, path ) {
	
	events.EventEmitter.call(this);

	var downloadDir = path + '/lib/';
	var tarFile 	= downloadDir + 'ffmpeg' + (platform === 'windows' ? '.zip' : '.tar.gz');
	var dstPath 	= downloadDir + 'ffmpeg';
	var percent 	= 0;

	this.emit('progress', percent += 5);
	var self = this;

	// There are 4 steps in the download progress, 
	// Extract -  5% 
	// Download- 75%
	// Extract - 15%
	// Clean   -  5%
	async.waterfall([
		// If it's windows, extract curl command first
		// Extract -  5% 
		function( callback ) {
			self.emit('progress', percent += 5);
			if ( platform === 'windows' ) {
				
				self.emit('newState', 'extract', 'curl.zip');
				
				var zip = new AdmZip( path + '/lib/curl.zip');
				zip.extractEntryTo('usr/bin/curl.exe', path + '/lib/', false, true);

				self.emit('stateDone', 'extract', 'curl.zip');

				callback( null );
			} else {
				callback( null );
			}
		},

		// Download file
		// Download- 75%
		function( callback ) {
			self.emit('newState', 'download', 'ffmpeg');
			
			var command = (platform === 'windows') ? (path + '/lib/curl.exe') : 'curl';
			var curl = spawn( command, ['-#L', url, '-o', tarFile] );
			console.log('downloading', url);
			// Curl uses stderr as stdout
			// As the link is referred from the public one (given),
			// create the number of parsers needed
			// ####### 100.0%
			curl.stderr.on('data', function( data ) {
				var line = data.toString('utf8');
				line = parseFloat(line);
				if ( !isNaN(line) ) {
					// A = C = 0
					// B = 100
					// D = 75
					var prog = mapToRange( line, [0, 100.0], [0.0, 75.0] );
					if ( percent < prog ) {
						percent = prog;
					}
					self.emit('progress', percent);
					self.emit('stateProgress', 'download', 'ffmpeg', parseInt(line, 10));
				}
			});

			curl.on('close', function( code ) {
				self.emit('stateDone', 'download', 'ffmpeg');
				console.log('done!');
				callback( null );
			});
		},

		// Extract file
		// Extract - 15%
		function( callback ) {
			console.log('extracting...');
			self.emit('newState', 'extract', 'ffmpeg');

			// Windows uses a zipped file version, extract it
			if ( platform === 'windows' ) {
				var zip = new AdmZip( tarFile );
				zip.extractAllTo( dstPath, true);
				self.emit('progress', 'extract', 'ffmpeg', percent += 15);
				self.emit('stateDone', 'extract', 'ffmpeg');
				callback( null );
			} else {
				var tar = spawn( 'tar', ['xvf', tarFile], { cwd: downloadDir });
				var files = 0;
				var numFiles = (platform === 'linux')? 5 : 3;
				// Tar uses stderr as stdout
				// There are 3 or 4 files
				tar.stderr.on('data', function( data ) {
					// Parse progress
					var file = data.toString('utf8').split('/');
					file = file[file.length - 1];
					// If it is not a hidden file
					if ( file.indexOf('.') !== 0 ) {
						files += 1;
						var prog = mapToRange( files, [0, 100], [0, 15] );
						self.emit('stateProgress', 'extract', 'ffmpeg', (files * 100)/numFiles );
						self.emit('progress', 'extract', 'ffmpeg', percent += prog);
					}
					console.log('tar err', data.toString('utf8'));
				});

				tar.on('close', function( code ) {
					self.emit('stateDone', 'extract', 'ffmpeg');
					console.log('done!');
					callback( null );
				});
			}
		},

		// Clean
		// Clean   -  5%
		function( callback ) {
			console.log('cleaning...');
			self.emit('newState', 'clean', 'ffmpeg');
			fs.unlink(tarFile, function() {
				self.emit('stateDone', 'clean', 'ffmpeg');
				self.emit('progress', percent += 5);
				console.log('done!');
				callback( null, 'ok' );
			});
		}

	], function( err, result ) {
		if ( err ) {
			self.emit('err', err);
		} else {
			self.emit('progress', 100);
			self.emit('done');
		}
	});
};

Downloader.prototype.__proto__ = events.EventEmitter.prototype;
module.exports = function( platform, url, path ) {
	return new Downloader( platform, url, path );
};